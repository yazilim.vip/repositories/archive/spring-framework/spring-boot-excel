package vip.yazilim.excel.operation.example.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

/**
 * @author Mehmet Arif Emre Şen, maemresen@yazilim.vip
 * 01.03.2022
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Person {
    private String fullName;
    private LocalDate birthDate;
    private boolean maritalStatus;
    private int numberOfChild;
}
