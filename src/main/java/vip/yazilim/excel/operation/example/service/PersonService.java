package vip.yazilim.excel.operation.example.service;

import org.springframework.stereotype.Service;
import vip.yazilim.excel.operation.example.model.Person;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;

/**
 * @author Mehmet Arif Emre Şen, maemresen@yazilim.vip
 * 01.03.2022
 */
@Service
public class PersonService {

    private static final List<Person> PERSON_DATA = List.of(
            new Person(
                    "Emre Şen",
                    LocalDate.of(1997, Month.AUGUST, 25),
                    false,
                    0
            ),
            new Person(
                    "Burak Erkan",
                    LocalDate.of(1985, Month.SEPTEMBER, 19),
                    true,
                    1
            )
    );

    public List<Person> findAll() {
        return PERSON_DATA;
    }
}
