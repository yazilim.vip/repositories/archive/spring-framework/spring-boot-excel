package vip.yazilim.excel.operation.example.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import vip.yazilim.excel.operation.example.model.Person;
import vip.yazilim.excel.operation.example.service.PersonService;
import vip.yazilim.excel.operation.example.util.DateUtils;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Burak Erkan, burak@yazilim.vip
 * 1.03.2022
 */
@Slf4j
@Controller
@RequiredArgsConstructor
public class HomeController {

    private final PersonService personService;

    @GetMapping("/home")
    public String home() {
        return "home";
    }

    @GetMapping("/test/report")
    public void downloadTest(HttpServletResponse response) throws IOException {

        response.setContentType(MimeTypeUtils.APPLICATION_OCTET_STREAM_VALUE);

        String headerKey = HttpHeaders.CONTENT_DISPOSITION;

        String currentDateTime = DateUtils.getNowAsFormattedString(LocalDateTime.now());
        String headerValue = String.format("attachment; filename=persons_%s.xlsx", currentDateTime);
        response.setHeader(headerKey, headerValue);

        try (Workbook workbook = new XSSFWorkbook()) {
            CellStyle cellStyle = workbook.createCellStyle();
            cellStyle.setWrapText(true);

            Sheet sheet = workbook.createSheet("Persons");
            Row row = sheet.createRow(1);
            Cell cell = row.createCell(2);
            cell.setCellValue("Hello excel");
            cell.setCellStyle(cellStyle);

            sheet.autoSizeColumn(2);
            try (ServletOutputStream outputStream = response.getOutputStream()) {
                workbook.write(outputStream);
            }
        }
    }

    @GetMapping("/person/report")
    public String downloadPersonReport(HttpServletResponse response) throws IOException {

        List<Person> personList = personService.findAll();

        response.setContentType(MimeTypeUtils.APPLICATION_OCTET_STREAM_VALUE);

        String headerKey = HttpHeaders.CONTENT_DISPOSITION;

        String currentDateTime = DateUtils.getNowAsFormattedString(LocalDateTime.now());
        String headerValue = String.format("attachment; filename=persons_%s.xlsx", currentDateTime);
        response.setHeader(headerKey, headerValue);

        try (Workbook workbook = new XSSFWorkbook()) {

            // create sheet on workbook
            Sheet sheet = workbook.createSheet("Persons");

            // create header row
            Row headerRow = sheet.createRow(0);

            // create header cells
            CellStyle headerCellStyle = workbook.createCellStyle();
            headerCellStyle.setFillBackgroundColor(IndexedColors.YELLOW.getIndex());

            Font headerFont = workbook.createFont();
            headerFont.setBold(true);
            headerCellStyle.setFont(headerFont);


            // header cells
            Cell fullNameHeaderCell = headerRow.createCell(0);
            fullNameHeaderCell.setCellStyle(headerCellStyle);
            fullNameHeaderCell.setCellValue("Full Name");

            Cell birthDateHeaderCell = headerRow.createCell(1);
            birthDateHeaderCell.setCellStyle(headerCellStyle);
            birthDateHeaderCell.setCellValue("Birth Date");

            Cell maritalStatusHeaderCell = headerRow.createCell(2);
            maritalStatusHeaderCell.setCellStyle(headerCellStyle);
            maritalStatusHeaderCell.setCellValue("Marital Status");

            Cell numberOfChildHeaderCell = headerRow.createCell(3);
            numberOfChildHeaderCell.setCellStyle(headerCellStyle);
            numberOfChildHeaderCell.setCellValue("Number Of Child");

            int rowStartIndex = 1;
            for (Person person : personList) {
                String logPrefix = String.format("Person[%s]", person.getFullName());
                try {
                    Row row = sheet.createRow(rowStartIndex);

                    Cell fullNameCell = row.createCell(0);
                    fullNameCell.setCellValue(person.getFullName());

                    Cell birthDateCell = row.createCell(1);
                    birthDateCell.setCellValue(person.getBirthDate());

                    Cell maritalStatusCell = row.createCell(2);
                    maritalStatusCell.setCellValue(person.isMaritalStatus());

                    Cell numberOfChildCell = row.createCell(3);
                    numberOfChildCell.setCellValue(person.getNumberOfChild());
                    log.info(logPrefix + "::write into excel successfully");
                } catch (Exception e) {
                    log.error(logPrefix + "::write into excel failed", e);
                }
                rowStartIndex++;
            }

            sheet.autoSizeColumn(0);
            sheet.autoSizeColumn(1);
            sheet.autoSizeColumn(2);
            sheet.autoSizeColumn(3);

            try (ServletOutputStream outputStream = response.getOutputStream()) {
                workbook.write(outputStream);
            }
        }

        return "redirect:/home";
    }

    @PostMapping("/person/report")
    public String downloadPersonReport(@RequestParam("file") MultipartFile file) throws IOException {
        InputStream inputStream = file.getInputStream();
        try (Workbook workbook = new XSSFWorkbook(inputStream)) {
            Sheet personSheet = workbook.getSheet("Persons");
            int rowStartIndex = 1;
            for (int i = rowStartIndex; i < 10; i++) {
                String logPrefix = "Row[" + i + "]";
                try {
                    Row row = personSheet.getRow(i);
                    String fullName = row.getCell(0).getStringCellValue();
                    LocalDateTime birthDate = row.getCell(1).getLocalDateTimeCellValue();
                    boolean maritalStatus = row.getCell(2).getBooleanCellValue();
                    int numberOfChild = (int) row.getCell(3).getNumericCellValue();
                    log.info(logPrefix + "::parsed successfully::fullName: {}, birthDate: {}, maritalStatus: {}, numberOfChild: {}"
                            , fullName
                            , DateUtils.getNowAsFormattedString(birthDate)
                            , maritalStatus
                            , numberOfChild);
                } catch (Exception e) {
                    log.error(logPrefix + "::error on parse", e);
                }
            }
        }
        return "redirect:/home";
    }
}
