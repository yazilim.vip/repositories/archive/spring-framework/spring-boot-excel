package vip.yazilim.excel.operation.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExcelOperationExampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(ExcelOperationExampleApplication.class, args);
    }
}
