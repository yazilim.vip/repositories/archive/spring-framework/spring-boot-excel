package vip.yazilim.excel.operation.example.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author Mehmet Arif Emre Şen, maemresen@yazilim.vip
 * 01.03.2022
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class DateUtils {

    public static String getNowAsFormattedString(LocalDateTime localDateTime) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd_HH:mm:ss");
        return  formatter.format(localDateTime);
    }

}
